# Diario SUPRI

La intencionalidad de este diario es conseguir un log informal del proceso de estado de la impresora 3D, con la finalidad de transmitir el estado actual al resto de SUGUS.



### 29/03/19: Se ha actualizado la configuración 2.8

Entre las mejoras observadas, La cola del Calicat impreso al 50% tiene una forma con sentido, aunque todavía se ven hilos entre ella y la cabeza. Una proxima mejora a realizar sería la corrección de la retracción. (Quizás subir la velocidad a 150, y si eso sigue sin funcionar, aumentar la retracción)

En la imagen se marca con rojo el nuevo gato impreso, en comparación con el impreso hace 1 semana.

![improvements29](improvements29.png)

---




